"""
Helper functions for the Tor browser.

Test with "python3 tor_browser.py doctest".
The tests will start the tor-browser so you probably
want to use a tester that handles user interaction or
run the tests from the command line and answer prompts as needed.

goodcrypto.com converted from bash to python and added basic tests.
"""

import os
import re
import sys
import glob


# sanitize PATH before executing any other code
os.environ['PATH'] = '/usr/local/bin:/usr/bin:/bin'

TBB_INSTALL = '/usr/local/lib/tor-browser'
TBB_PROFILE = '/etc/tor-browser/profile'
TBB_EXT = '/usr/local/share/tor-browser-extensions'
TOR_LAUNCHER_INSTALL = '/usr/local/lib/tor-launcher-standalone'
TOR_LAUNCHER_LOCALES_DIR = os.path.join(TOR_LAUNCHER_INSTALL, 'chrome/locale')


def exec_unconfined_firefox(*args):
    """Execute Tor Browser unconfined

    This should open Tor Browser and display a tempfile with the content "secret"
    >>> PROFILE = '{}/.tor-browser/profile.default'.format(os.environ['HOME'])
    >>> from multiprocessing import Process
    >>> from tempfile import NamedTemporaryFile
    >>> with NamedTemporaryFile('w') as f:
    ...     _ = f.write("secret")
    ...     f.flush()
    ...     args = ('-allow-remote', '--class', 'Tor Browser', '-profile', PROFILE, f.name)
    ...     p = Process(target=exec_unconfined_firefox, args=args)
    ...     p.start()
    ...     p.join()
    ...     p.exitcode == 0
    True
    """

    return exec_firefox(*args, unconfined=True)


def exec_firefox(*args, unconfined=False):
    """Set environment variables and execute Tor Browser

    This should open Tor Browser and display a "Access to the file was denied" error
    >>> PROFILE = '{}/.tor-browser/profile.default'.format(os.environ['HOME'])
    >>> from multiprocessing import Process
    >>> from tempfile import NamedTemporaryFile
    >>> with NamedTemporaryFile('w') as f:
    ...     args = ('-allow-remote', '--class', 'Tor Browser', '-profile', PROFILE, f.name)
    ...     p = Process(target=exec_firefox, args=args)
    ...     p.start()
    ...     p.join()
    ...     p.exitcode == 0
    True
    """

    binary = 'firefox-unconfined' if unconfined else 'firefox'

    os.environ['LD_LIBRARY_PATH'] = TBB_INSTALL
    os.environ['FONTCONFIG_PATH'] = os.path.join(TBB_INSTALL, 'TorBrowser/Data/fontconfig')
    os.environ['FONTCONFIG_FILE'] = 'fonts.conf'
    os.environ['GNOME_ACCESSIBILITY'] = '1'

    # The Tor Browser often assumes that the current directory is
    # where the browser lives, e.g. for the fixed set of fonts set by
    # fontconfig above.
    os.chdir(TBB_INSTALL)

    # From start-tor-browser:
    os.unsetenv('SESSION_MANAGER')
    os.environ['SELFRANDO_write_layout_file'] = ''
    binary_path = os.path.join(TBB_INSTALL, binary)
    full_args = [binary_path] + list(args)

    os.execv(binary_path, full_args)


def set_mozilla_pref(filename, name, value, prefix='pref'):
    """Set mozilla preference.

    For strings it's up to the caller to add
    double-quotes ("") around the value.

    Sometimes we might want prefix to be e.g. user_pref

    >>> IMAP = 1
    >>> PROFILE = '{}/.tor-browser/profile.default'.format(os.environ['HOME'])
    >>> PREFERENCES_DIR = os.path.join(PROFILE, 'preferences')
    >>> os.makedirs(PREFERENCES_DIR, exist_ok=True)
    >>> TAILS_JS = os.path.join(PREFERENCES_DIR, '0000tails.js')
    >>> lines = set_mozilla_pref(TAILS_JS, "extensions.torbirdy.defaultprotocol", IMAP)
    >>> len(lines) > 0
    True
    """

    new_lines = []
    prefixed_name = '{prefix}("{name}"'.format(prefix=prefix, name=name)

    # delete old lines for this setting
    if os.path.isfile(filename):
        with open(filename) as f:
            new_lines = [line for line in f if not line.startswith(prefixed_name)]

    # add the new setting
    new_lines.append('{prefix}("{name}", {value});\n'.format(prefix=prefix, name=name, value=value))
    with open(filename, 'w') as f:
        f.writelines(new_lines)

    # returned for doctest
    return new_lines


def guess_best_tor_browser_locale():
    """Get the best Tor browser locale.

    >>> guess_best_tor_browser_locale()
    'en-US'
    """
    long_locale = get_long_locale()
    short_locale = get_short_locale(long_locale)

    lang_pack_path = os.path.join(TBB_EXT, 'langpack-{}@firefox.mozilla.org.xpi')

    if os.path.exists(lang_pack_path.format(long_locale)):
        return long_locale

    if os.path.exists(lang_pack_path.format(short_locale)):
        return short_locale

    # If we use locale xx-YY and there is no langpack for xx-YY nor xx
    # there may be a similar locale xx-ZZ that we should use instead.
    similar_locales = glob.glob(lang_pack_path.format(short_locale + '-*'))
    if similar_locales:
        return similar_locales[0]

    return 'en-US'


def guess_best_tor_launcher_locale():
    """Get the best Tor launcher locale.

    >>> guess_best_tor_launcher_locale()
    'en-US'
    """
    long_locale = get_long_locale()
    short_locale = get_short_locale(long_locale)

    if os.path.exists(os.path.join(TOR_LAUNCHER_LOCALES_DIR, long_locale)):
        return long_locale

    if glob.glob(os.path.join(TOR_LAUNCHER_LOCALES_DIR, short_locale + '*')):
        return short_locale

    return 'en-US'


def configure_best_tor_browser_locale(profile):
    """Configure the best Tor browser locale.

    >>> PROFILE = '{}/.tor-browser/profile.default'.format(os.environ['HOME'])
    >>> configure_best_tor_browser_locale(PROFILE)
    """
    best_locale = guess_best_tor_browser_locale()
    configure_xulrunner_app_locale(profile, best_locale)
    with open("{}/preferences/0000locale.js".format(profile), "a") as f_out:
        with open("/etc/tor-browser/locale-profiles/{}.js".format(best_locale)) as f_in:
            f_out.write(f_in.read())


def configure_best_tor_launcher_locale(profile):
    """Configure the best Tor launcher locale.

    >>> PROFILE = '{}/.tor-browser/profile.default'.format(os.environ['HOME'])
    >>> configure_best_tor_launcher_locale(PROFILE)
    """
    configure_xulrunner_app_locale(profile, guess_best_tor_launcher_locale())


def configure_xulrunner_app_locale(profile, locale):
    """Configure XUL runner app's locale.

    >>> PROFILE = '{}/.tor-browser/profile.default'.format(os.environ['HOME'])
    >>> js_path = os.path.join(PROFILE, 'preferences/0000locale.js')
    >>> if os.path.exists(js_path):
    ...     os.remove(js_path)
    >>> configure_xulrunner_app_locale(PROFILE, 'de-CH')
    >>> os.path.exists(js_path)
    True
    >>> with open(js_path) as f:
    ...     print(f.read())
    pref("general.useragent.locale", "de-CH")
    """
    prefs_dir = os.path.join(profile, 'preferences')
    if not os.path.exists(prefs_dir):
        os.makedirs(prefs_dir)

    js_path = os.path.join(profile, 'preferences/0000locale.js')
    with open(js_path, 'w') as js_file:
        js_file.write('pref("general.useragent.locale", "{}")'.format(locale))


def supported_tor_browser_locales():
    """Determine which locales are supported by the Tor Browser.

    >>> locales = supported_tor_browser_locales()
    >>> len(locales) > 1
    True
    """
    # The default is always supported
    locales = ['en-US']
    for langpack in glob.glob(os.path.join(TBB_EXT, 'langpack-*@firefox.mozilla.org.xpi')):
        basename = os.path.basename(langpack)
        reg_ex = r'^langpack-([^@]+)@firefox\.mozilla\.org\.xpi$'
        match = re.match(reg_ex, basename)
        if match:
            locales.append(match.group(1))

    return locales


def get_long_locale():
    """Get the long locale from the OS.

    >>> locale = get_long_locale()
    >>> len(locale) > 0
    True
    >>> '_' in locale
    False
    """
    lang = os.environ['LANG']
    parts = lang.split('.')
    return parts[0].replace('_', '-')


def get_short_locale(long_locale):
    """Get the short locale from the OS.

    >>> get_short_locale('de-CH')
    'de'
    """
    parts = long_locale.split('-')
    return parts[0]


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'doctest':
        import doctest
        doctest.testmod()
